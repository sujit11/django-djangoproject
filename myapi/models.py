# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.
class Hero(models.Model):
	name = models.CharField(max_length=60)
	alias = models.CharField(max_length=60)

	def __str__(self):
		return self.name


class Restaurant(models.Model):
	resturantName = models.CharField(max_length=30, blank=True)
	location = models.CharField(max_length=255, blank=True)
	phoneNo = models.CharField(max_length=20, blank=True)
	dzongkhagId = models.IntegerField(blank=True, null=True)
	imageUrl = models.CharField(max_length=30)
	description = models.CharField(max_length=100, blank=True)
	createdDate = models.DateField(blank=True, null=True)
	status = models.CharField(max_length=1, blank=True)
	closeDay = models.CharField(max_length=10, blank=True)
	closingTime = models.TimeField(blank=True, null=True)
	packagingCharge = models.IntegerField(blank=True, null=True)
	isBakery = models.BooleanField(blank=True, null=True)
	isCateringService = models.BooleanField(blank=True, null=True)
	canCustomized = models.BooleanField(blank=True, null=True)

	def __str__(self):
		return self.resturantName
