# serializers.py
from rest_framework import serializers

from .models import Hero
from .models import Restaurant

class HeroSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Hero
        fields = ('name', 'alias')

class RestaurantSerializer(serializers.HyperlinkedModelSerializer):
	class Meta:
		model = Restaurant
		fields = ('resturantName', 'location', 'phoneNo', 'dzongkhagId', 'imageUrl', 'description', 'createdDate',
				  'status', 'closeDay', 'closingTime', 'packagingCharge', 'isBakery', 'isCateringService',
				  'canCustomized')
